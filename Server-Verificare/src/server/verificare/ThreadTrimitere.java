package server.verificare;
/**
 *
 * @author XYZ
 */
import java.io.*;
import java.net.*;
import java.util.Scanner;

class ThreadTrimitere extends Thread {

    Socket newsock;
    int n;
    private static String retineCom = "";

    ThreadTrimitere(Socket s, int v) {
        newsock = s;
        n = v;
    }

    public void run() {
        try {

            PrintWriter outp = new PrintWriter(newsock.getOutputStream(), true);
            outp.println("Hello :: enter QUIT to exit \n");
            boolean more_data = true;
            //String raspuns ="";
            //Scanner r = new Scanner(System.in);

            while (more_data && newsock.isConnected()) {
//            System.out.println("Raspunde catre client: ");
//                if(r.hasNext()){
//                    raspuns=r.next();
//                }
//            
                if (ThreadTrimitere.retineCom.equals("QUIT")) {
                    System.out.println("QUIT");
                    more_data = false;
                    ThreadTrimitere.retineCom = "";
                }

                if (ThreadTrimitere.retineCom.equals(ThreadHandler.requestFiles)) {
                    Fisiere.cauta();
                    System.out.println("Ceva!");
                    outp.println("Nr Fisiere: "+Fisiere.count+"\n");
                   outp.flush();
                    for (int i = 0; i < Fisiere.getRecord().length; i++) {
                        if (Fisiere.getRecord()[i] != null) {
                            outp.print("Fisier:");
                            //System.out.println(Fisiere.getRecord()[i]); 
                            outp.println(Fisiere.getRecord()[i]);
                          
                           outp.flush();
                        }
                    }
                    ThreadTrimitere.retineCom = "";
//                        System.out.println("From server: " + retineCom + ". \n");
//                        
                }
                Thread.sleep(10);
            }
            System.out.println("Disconnected thread trimitere from client number: " + n);
        } catch (Exception e) {
            System.out.println("IO error: " + e);
        }

    }

    //reporneste threadul cand clientul se deconecteaza
    public static void setCom(String x) {

        ThreadTrimitere.retineCom = x;
        System.out.println("Primit comanda: " + ThreadTrimitere.retineCom);
    }
}

package server.verificare;


/**
 *
 * @author XYZ
 */
import java.io.*;
import java.net.*;
import java.util.Scanner;

class ThreadHandler extends Thread {
    Socket newsock;
    int n;
    public final static String requestFiles = "RequestFile";
    ThreadHandler(Socket s, int v) {
        newsock = s;
        n = v;
    }

    
    public void run() {
        try {
            BufferedReader inp = new BufferedReader(new InputStreamReader(
                    newsock.getInputStream()));
            boolean more_data = true;
            String line="";


            while (more_data) {
                line = inp.readLine();
                System.out.println("Message from client: " + line);
            
                if (line == null) {
                    System.out.println("line = null");
                    more_data = false;
                
                }
                else if(line.equals(requestFiles)){
                    
                    //System.out.println(requestFiles);
                    ThreadTrimitere.setCom(requestFiles);
                    line = "";
                    
                }
                else if(line.contains("SendFile:")){
                
                    System.out.println(line);
                    System.out.println(line.substring(9));
                    Fisiere.continut(line.substring(9));
                    
                    
                }
                else {
                    if (line.trim().equals("QUIT") ){
                        ThreadTrimitere.setCom("QUIT");
                        more_data = false;
                    }
                }
                Thread.sleep(10);
            }
            newsock.close();
            System.out.println("Disconnected  thread primire from client number: " + n);
        } catch (Exception e) {
            System.out.println("IO error: " + e);
        }

    }
}
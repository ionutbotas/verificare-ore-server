
import java.util.Formatter;

/**
 *
 * @author XYZ
 */
public class CreazaFisier {
    
    private Formatter a;
    public String nume="test.txt";
    
    public void creazaFisier(){
        try{
        
            a=new Formatter(nume);
            System.out.println("Fisier '" + nume + "' creat!");
        
        }catch(Exception e){
        e.printStackTrace();
        }
        
    }
    public void scrieInFisier(){
        try{
        a.format("%s%s%s","Data: \n", "Progres: \n","Locatie: \n");
        }catch(Exception e){
            System.out.println("Eroare la scriere: "+e);
        }
    }
    public void inchideFisier(){
    
        try{
        a.close();
        }catch(Exception e){
            System.out.println("Eroare la inchidere: "+e);
        }
        
    }
}

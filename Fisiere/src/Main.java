
import java.util.Formatter;
import java.util.Scanner;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author XYZ
 */
public class Main {

    public static void main(String args[]) {

        try {
            
            CreazaFisier i1 = new CreazaFisier();
            CitesteFisier c = new CitesteFisier();
            i1.creazaFisier();
            i1.scrieInFisier();
            i1.inchideFisier();
            c.deschideFisier();
            c.citesteFisier();
            String nume = "testObiect.bin";
            
            Computer a = new Computer();
            a.cpu = "intel";
            a.gpu = "AMD";
            a.hdd = 250;
            a.ram = 8;
            
            ObjectOutputStream output;
            try {
                output = new ObjectOutputStream(new FileOutputStream(nume));
                output.writeObject(a);
                output.close();
            } catch (Exception e) {
                System.out.println(e);
            }
            ObjectInputStream in; 
            in = new ObjectInputStream(new FileInputStream(nume));
            Computer x =(Computer) in.readObject();
            System.out.println(x.cpu+"\n"+x.gpu+"\n"+x.hdd+"\n"+x.ram);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
